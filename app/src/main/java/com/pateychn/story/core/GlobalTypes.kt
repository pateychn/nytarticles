package com.pateychn.story.core

import com.google.gson.annotations.SerializedName

/**
 * Images format
 */
enum class Format {
    @SerializedName("Standard Thumbnail") STANDARD,
    @SerializedName("thumbLarge") LARGE,
    @SerializedName("Normal") NORMAL,
    @SerializedName("mediumThreeByTwo210") MEDIUM_THREE_BY_TWO,
    @SerializedName("superJumbo") SUPER_JUMBO
}