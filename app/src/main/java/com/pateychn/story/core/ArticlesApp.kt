package com.pateychn.story.core

import android.app.Application

/**
 * Application management
 */
class ArticlesApp : Application() {

    companion object {

        private var instance: ArticlesApp? =null

        // For easy access to context anywhere
        fun getContext() = instance!!.applicationContext

    }

    init {
        instance = this
    }

}