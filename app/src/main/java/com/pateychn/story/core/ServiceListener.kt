package com.pateychn.story.core

import com.pateychn.story.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

//
// Helpers for retrofit API
//

/**
 * Execute an async call
 */
fun <T> Call<T>.async(initCallback: ServiceListener<T>.() -> Unit) {
    return enqueue(ServiceListener(initCallback))
}

/**
 * Execute a sync call
 */
fun <T> Call<T>.get(): T? {
    return execute().body()
}

/**
 * Callback adapter for retrofit calls
 * Hides retrofit library from the calling service layer
 *
 * Allows following kotlin syntax for asynchronous calls :
 * fun get() {
 *      onSuccess {...}
 *      onError {...}
 * }
 *
 */
class ServiceListener<T>(initCallbacks: ServiceListener<T>.() -> Unit) : Callback<T> {

    private var onCallbackSuccess: (result: T?) -> Unit = {}
    private var onCallbackError: (msg: String?) -> Unit = {}

    init {
        initCallbacks()
    }

    /**
     * Sets callback function for successful response
     *
     * @param body of the success function to execute
     */
    fun onSuccess(body: (result: T?) -> Unit) {
        onCallbackSuccess = body
    }

    /**
     * Calls onCallbackSuccess when on response is called from retrofit
     */
    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        response?.let { response ->
            if ( response.isSuccessful ) {
                onCallbackSuccess(response.body())
            } else {
                onCallbackError(response.message())
            }
        }
    }

    /**
     * Sets callback function for error response
     *
     * @param body of the error function to execute
     */
    fun onError(body: (msg: String?) -> Unit) {
        onCallbackError = body
    }

    /**
     * Calls onCallbackError when onFailure is called from retrofit
     */
    override fun onFailure(call: Call<T>?, t: Throwable?) {
        onCallbackError(t?.message ?: ArticlesApp.getContext().getString(R.string.unknown_error))
    }

}