package com.pateychn.story.core

import com.pateychn.story.dto.ImageDto
import com.pateychn.story.dto.ItemDto
import com.pateychn.story.service.nyt.Multimedia
import com.pateychn.story.service.nyt.Result

/**
 * Conversion tools from model to dto
 */
fun Result.toItemDto() = ItemDto(title, abstract, url, byline, publishedDate, multimedia.map { it.toImageDto() })

fun Multimedia.toImageDto() = ImageDto(url, type, format, width, height)
