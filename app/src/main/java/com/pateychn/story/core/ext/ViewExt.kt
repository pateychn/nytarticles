package com.pateychn.story.core

import android.content.res.TypedArray
import android.support.annotation.StyleableRes
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.View
import kotlin.math.round

//
// Kotlin utilities for views
//


/**
 * Convert xdp to pixels
 */
fun View.getPixelWidthFromDp(dp: Int): Int {
    return round(resources.displayMetrics.xdpi * dp / DisplayMetrics.DENSITY_DEFAULT).toInt()
}

/**
 * Convert ydp to pixels
 */
fun View.getPixelHeightFromDp(dp: Int): Int {
    return round(resources.displayMetrics.ydpi * dp / DisplayMetrics.DENSITY_DEFAULT).toInt()
}

/**
 * Sets z translation of a view for post LOLLIPOP or none
 */
fun View.setZTranslation(z: Float) {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
        this.translationZ = z
    }
}

/**
 * Apply a function on a typed array for custom view attributes reading
 */
inline fun View.withStyleableRes(set: AttributeSet, @StyleableRes attrs: IntArray, init: TypedArray.() -> Unit) {
    val typedArray = context.theme.obtainStyledAttributes(set, attrs, 0, 0)
    if ( typedArray != null ) {
        try {
            typedArray.init()
        } finally {
            typedArray.recycle()
        }
    }
}
