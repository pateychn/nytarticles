package com.pateychn.story.core

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

//
// Kotlin utilities for activity
//

/**
 * Returns a viewmodel of generic type T
 */
inline fun <reified T: ViewModel> FragmentActivity.getViewModel(): T {
    return ViewModelProviders.of(this).get(T::class.java)
}

/**
 * Observe a liveData
 *
 * @param liveData to observe
 * @param body : function to execute when the data is updated
 */
inline fun <T> FragmentActivity.observe(liveData: LiveData<T>, crossinline body: (T?) -> Unit) {
    liveData.observe(this, Observer { body(liveData.value) })
}

/**
 * Fragment transaction shortform
 *
 * @param transaction : body of the fragment transaction
 */
inline fun FragmentManager.fragmentTransaction(transaction: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.transaction()
    fragmentTransaction.commit()
}


