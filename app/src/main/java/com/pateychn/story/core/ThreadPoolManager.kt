package com.pateychn.story.core

/**
 * Created by p.eychenne on 19/04/2017.
 */

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Thread pool management
 */

object ThreadPoolManager {
    private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()
    private val KEEP_ALIVE_TIME = 1
    private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS

    private val mWorkerThreadPool: ThreadPoolExecutor = ThreadPoolExecutor(
            NUMBER_OF_CORES,
            NUMBER_OF_CORES,
            KEEP_ALIVE_TIME.toLong(),
            KEEP_ALIVE_TIME_UNIT,
            LinkedBlockingQueue<Runnable>())

    /**
     * Add Runnable to threadpool executor
     * @param runnable
     */
    fun execute(runnable: Runnable) {
        mWorkerThreadPool.execute(runnable)
    }
}

fun async (body: () -> Unit) = ThreadPoolManager.execute(Runnable { body() })

