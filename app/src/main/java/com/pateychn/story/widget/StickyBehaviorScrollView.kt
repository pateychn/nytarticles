package com.pateychn.story.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ScrollView
import com.pateychn.story.R
import com.pateychn.story.core.setZTranslation
import com.pateychn.story.core.withStyleableRes


/**
 * ScrollView for PASS W'in list
 *
 * Apply two effects :
 *
 * Sticky header view
 * Fading a view behind the sticky view and the bottom of the scrolling zone
 */
class StickyBehaviorScrollView : ScrollView {

    private val UNDEFINED_ATTRIBUTE = -1
    private var stickyHeaderView: View? = null
    private var fadingView: View? = null
    private var stickyHeaderIdAttribute = 0
    private var fadingViewIdAttribute = 0

    private var yFadingStartBiasFromTopAttribute = 0f
    private var fadingStartScrollPosition = 0f
    private var initialStickyViewTopPosition = 0f

    private var startFadingDist = 0f
    private var endFadingDist = 0f

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttributes(attrs)
    }

    /**
     * Initialize attributes for the scrolling animation :
     *
     * stickyHeaderIdAttribute : resourceId of the sticky header view
     * fadingViewIdAttribute : resourceId of the fading view
     * yFadingStartBiasFromTopAttribute : position in pecrent from the top of the screen
     * for starting the fading view effect
     */
    private fun initAttributes(attrs: AttributeSet) {

        withStyleableRes(attrs, R.styleable.StickyBehaviorScrollView) {
            stickyHeaderIdAttribute = getResourceId(R.styleable.StickyBehaviorScrollView_stickyHeaderChild, UNDEFINED_ATTRIBUTE)
            fadingViewIdAttribute = getResourceId(R.styleable.StickyBehaviorScrollView_fadingChild, UNDEFINED_ATTRIBUTE)
            yFadingStartBiasFromTopAttribute = getFloat(R.styleable.StickyBehaviorScrollView_fadingStartBiasFromTop, 0.35f)
        }

        checkAttributes()

    }

    /**
     * Check attributes values
     * Throws IllegalArgumentException if any value is incorrect
     */
    private fun checkAttributes() {
        if (yFadingStartBiasFromTopAttribute !in 0..1) throw IllegalArgumentException("FadingStartBias must be included in 0..1 range")
    }

    /**
     * Handle onLayout event to initialize view positions & parameters used for animations
     */
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        stickyHeaderView = if ( stickyHeaderIdAttribute != UNDEFINED_ATTRIBUTE  ) findViewById(stickyHeaderIdAttribute) else null

        stickyHeaderView?.let { stickyView ->
            fadingView = if ( fadingViewIdAttribute != UNDEFINED_ATTRIBUTE ) findViewById(fadingViewIdAttribute) else null
            initialStickyViewTopPosition = stickyView.getTopPosition()
            startFadingDist = initialStickyViewTopPosition - fadingStartScrollPosition
            endFadingDist = initialStickyViewTopPosition
            fadingStartScrollPosition = context.resources.displayMetrics.heightPixels * yFadingStartBiasFromTopAttribute
        }

    }

    /**
     * Handle scroll event to apply animation depending on scrollY value
      */
    override fun onScrollChanged(scrollX: Int, scrollY: Int, oldl: Int, oldt: Int) {
        applyStickyHeaderEffect(scrollY)
        super.onScrollChanged(scrollX, scrollY, oldl, oldt)
    }

    /**
     * Compute sticky header position from the top of the screen :
     *
     * When scrollY has consumed initialStickyViewTopPosition distance,
     * the sticky view has reached the top of the screen. In this case
     * the sticky view is translated on Y axis of (scrollY - initialStickyViewTopPosition)
     * distance
     *
     * When scrollY returns below initialStickyViewTopPosition distance
     * the sticky view translation is reset to zero
     *
     * @param: scrollY is the scrolled distance from onScrollChanged event
     */
    private fun applyStickyHeaderEffect(scrollY: Int) {
        stickyHeaderView?.apply {
            if (scrollY > initialStickyViewTopPosition) {
                translationY = scrollY - initialStickyViewTopPosition
                setZTranslation(1f)
            } else {
                translationY = 0f
                setZTranslation(0f)
            }
            computeFadingViewEffect(scrollY)
        }
    }

    /**
     * Compute effects for for the fading view :
     * - alpha value
     * - translation on Y axis
     *
     * When scrollY value has reached the screen zone corresponding to
     * yFadingStartBiasFromTopAttribute the alpha value of the fading view
     * decrease from 1 to 0 and the scroll speed is reduced (by increasing
     * the translation on Y axis)
     *
     * @param: scrollY is the scrolled distance from onScrollChanged event
     */
    private fun computeFadingViewEffect(scrollY: Int) {
        fadingView?.apply {
            if (scrollY > startFadingDist && scrollY < endFadingDist) {
                alpha = (endFadingDist - scrollY) / (endFadingDist - startFadingDist)
                translationY = scrollY.toFloat() / 2
                setZTranslation(-1f)
            } else {
                alpha = 1f
                translationY = 0f
                setZTranslation(0f)
            }
        }
    }

    /**
     * Calculate the top position of a view in the PassWinScrollView
     * from it's top position and it's ancestors top
     *
     * @return position from the top
     */
    private fun View.getTopPosition(): Float =
            if ( this.parent is StickyBehaviorScrollView) {
                this.top.toFloat()
            } else {
                this.top.toFloat() + (parent as View).getTopPosition()
            }
}
