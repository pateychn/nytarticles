package com.pateychn.story.article.list

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.pateychn.story.R
import com.pateychn.story.article.ListItemClickListener
import com.pateychn.story.core.Format
import com.pateychn.story.core.getPixelHeightFromDp
import com.pateychn.story.core.getPixelWidthFromDp
import com.pateychn.story.dto.ItemDto

/**
 * Adapter for articles recyclerview
 */
class ArticlesListAdapter(var fragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var itemDto: ItemDto
    private var articlesList: MutableList<ItemDto> = mutableListOf()

    /**
     * Adds articles
     */
    fun setData(items: MutableList<ItemDto>) {
        this.articlesList = items
        notifyDataSetChanged()
    }

    /**
     * Clear adapter data
     */
    fun clear() {
        articlesList.clear()
        notifyDataSetChanged()
    }

    /**
     * Article viewHolder provider
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_articles_list_item, parent, false)
        return ArticleViewHolder(view)
    }

    /**
     * When ProgressViewHolder is binded to a view, throw loadNextPage to add next articles
     */
    override fun onBindViewHolder(holderArticle: RecyclerView.ViewHolder, position: Int) {
        if (holderArticle is ArticleViewHolder) {
            itemDto = articlesList[position]
            Log.d("DDDD", itemDto.title)
            holderArticle.bind(itemDto)
        }
    }

    override fun getItemCount(): Int {
        return articlesList.size
    }

    /**
     * Article viewHolder
     */
    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title = itemView.findViewById<TextView>(R.id.articleTitle)
        private val abstract = itemView.findViewById<TextView>(R.id.articleSection)
        private val image = itemView.findViewById<ImageView>(R.id.articleImage)

        private val imageWidth = itemView.getPixelWidthFromDp(100)
        private val imageHeight = itemView.getPixelHeightFromDp(100)

        fun bind(itemDto: ItemDto) {

            title.text = itemDto.title
            abstract.text = itemDto.abstract

            itemDto.images?.firstOrNull { it.format == Format.NORMAL }?.url?.let {
                Glide.with(fragment)
                        .load(it)
                        .placeholder(R.drawable.ic_image_placeholder_100dp)
                        .error(R.drawable.ic_broken_image_100dp)
                        .override(imageWidth, imageHeight)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .centerCrop()
                        .crossFade()
                        .into(image)
            }

            itemView.setOnClickListener { (fragment as ListItemClickListener).onItemClicked(layoutPosition) }

        }

    }

}
