package com.pateychn.story.article

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.pateychn.story.core.async
import com.pateychn.story.core.toItemDto
import com.pateychn.story.dto.ItemDto
import com.pateychn.story.service.nyt.NYTService


/**
 * Viewmodel for Article activity
 *
 * Share between list view and detail view
 */
class ArticlesViewModel : ViewModel() {

    /**
     * Current articles list displayed
     */
    val articles: MutableLiveData<List<ItemDto>> = getData()

    /**
     * Current position in list view
     */
    var listPosition: MutableLiveData<Int> = MutableLiveData()

    var currentArticle: LiveData<ItemDto> =
            Transformations.switchMap(listPosition) {
                getArticleFromPosition(it)
            }

    /**
     * Loads a articlesListPage of articles list
     */
    private fun getData(): MutableLiveData<List<ItemDto>> {

        val liveData = MutableLiveData<List<ItemDto>>()

        NYTService.getService()
                .getData()
                .async {
                    onSuccess { response ->
                        if (response?.results != null) {
                            val articlesList = response.results.toTypedArray()
                            liveData.value = articlesList.map { it.toItemDto() }
                        }
                    }
                }

        return liveData
    }

    private fun getArticleFromPosition(position: Int): MutableLiveData<ItemDto> {
        val nbArticles = articles.value?.size
        val article = MutableLiveData<ItemDto>()
        try {
            article.value = articles.value?.get(position)
        } catch (e: ArrayIndexOutOfBoundsException) {
            Log.d("DDD", "IndexOutOfBoundsException : article.size = $nbArticles position = $position" )
        }
        return article
    }

}