package com.pateychn.story.article

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.pateychn.story.R
import com.pateychn.story.article.list.ArticlesListFragment
import com.pateychn.story.article.page.ArticleDetailFragment
import com.pateychn.story.core.fragmentTransaction
import kotlinx.android.synthetic.main.activity_articles_list.*


/**
 * Display articles list on mobile
 * Display the reusable fragment ArticlesListFragment
 */
class ArticleActivity : AppCompatActivity(), ListItemClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_articles_list)

        setSupportActionBar(toolbar)

        if (supportFragmentManager.fragments.size == 0) {
            displayList()
        }

    }

    /**
     * Manage clic event on an item in the article list
     *
     * @param position : layout position
     */
    override fun onItemClicked(position: Int) {
        displayDetail()
    }

    /**
     * Display articles list fragment
     */
    private fun displayList() {
        // Attach articleItem list fragment
        supportFragmentManager.fragmentTransaction {
            val fragment = ArticlesListFragment.getInstance()
            setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit)
            replace(R.id.contentPanel, fragment)
        }
    }

    /**
     * Display article articlesListPage fragment
     */
    private fun displayDetail() {
        supportFragmentManager.fragmentTransaction {
            val fragment = ArticleDetailFragment.getInstance()
            setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit)
            replace(R.id.contentPanel, fragment)
            addToBackStack(null)
        }
    }

    /**
     * Manage header icon clic (available with articlesListPage fragment only) :
     * pop the articlesListPage fragment to return to the list
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                supportFragmentManager.apply {
                    overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit)
                    popBackStack()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}

