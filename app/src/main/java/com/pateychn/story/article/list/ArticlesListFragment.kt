package com.pateychn.story.article.list

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pateychn.story.R
import com.pateychn.story.article.ArticlesViewModel
import com.pateychn.story.article.ListItemClickListener
import com.pateychn.story.core.getViewModel
import com.pateychn.story.core.observe
import com.pateychn.story.dto.ItemDto

/**
 * Display articles list
 */
class ArticlesListFragment : Fragment(), ListItemClickListener {

    private lateinit var viewModel: ArticlesViewModel
    private lateinit var articlesRecyclerView: RecyclerView

    private val articlesAdapter: ArticlesListAdapter by lazy {
        ArticlesListAdapter(this)
    }

    /**
     * Fragment instantiation
     */
    companion object {
        fun getInstance(): ArticlesListFragment {
            return ArticlesListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_articles_list, container, false)
    }

    /**
     * Init view & observe data
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val appCompatActivity = activity
        if (appCompatActivity is AppCompatActivity) {
            appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }

        // Recyclerview initialization
        val fragmentActivity = activity
        if ( fragmentActivity is FragmentActivity) {
            articlesRecyclerView = fragmentActivity.findViewById(R.id.articlesRecyclerView)
            LinearLayoutManager(fragmentActivity).let { layoutManager ->
                articlesRecyclerView.layoutManager = layoutManager
                articlesRecyclerView.addItemDecoration(DividerItemDecoration(fragmentActivity, layoutManager.orientation))
            }

            // Attach viewModel
            viewModel = fragmentActivity.getViewModel()

            // React to articles update
            fragmentActivity.observe(viewModel.articles) {
                displayArticles(it)
            }

            // Set position
            fragmentActivity.observe(viewModel.listPosition) {
                scrollToArticle(it)
            }
        }


    }

    /**
     * Position recyclerview on the selected article
     */
    private fun scrollToArticle(position: Int?) {
        position?.let {
            articlesRecyclerView.scrollToPosition(it)
        }
    }

    /**
     * Sends articleItem list to the adapter
     *
     * @param articlesList : Dto articles list to display
     */
    private fun displayArticles(articlesList: List<ItemDto>?) {
        articlesList?.let {
            if (articlesRecyclerView.adapter==null) articlesRecyclerView.adapter = articlesAdapter
            articlesAdapter.setData(it.toMutableList())
        } ?: articlesAdapter.clear()
    }

    /**
     * Notify viewmodel when position change
     */
    override fun onItemClicked(position: Int) {
        (activity as ListItemClickListener).onItemClicked(position)
        viewModel.listPosition.value = position
    }

}