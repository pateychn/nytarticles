package com.pateychn.story.article

/**
 * Listener interface to handle next articles list articlesListPage loading
 */
interface ArticleLoader {

    fun loadNextPage()

}