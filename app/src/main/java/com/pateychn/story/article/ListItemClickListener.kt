package com.pateychn.story.article

/**
 * Listener to handle clic event on articles list
 */
interface ListItemClickListener {

    fun onItemClicked(position: Int)

}