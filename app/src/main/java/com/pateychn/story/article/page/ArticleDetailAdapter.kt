package com.pateychn.story.article.page

import android.os.Handler
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pateychn.story.R
import com.pateychn.story.core.Format
import com.pateychn.story.core.async
import com.pateychn.story.dto.ItemDto
import org.jsoup.Jsoup
import java.net.SocketTimeoutException


/**
 * Adapter for articles recyclerview
 */
class ArticleDetailAdapter(var fragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var itemDto: ItemDto
    private var articlesList: MutableList<ItemDto> = mutableListOf()

    /**
     * Adds articles
     */
    fun setData(items: MutableList<ItemDto>) {
        this.articlesList = items
        notifyDataSetChanged()
    }

    /**
     * Clear adapter data
     */
    fun clear() {
        articlesList.clear()
        notifyDataSetChanged()
    }

    /**
     * Article view provider
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.article_paged_item, parent, false)
        return ArticleViewHolder(view)
    }

    /**
     * When ProgressViewHolder is binded to a view, throw loadNextPage to add next articles
     */
    override fun onBindViewHolder(holderArticle: RecyclerView.ViewHolder, position: Int) {
        if (holderArticle is ArticleViewHolder) {
            itemDto = articlesList[position]
            holderArticle.bind(itemDto)
        }
    }

    override fun getItemCount(): Int {
        return articlesList.size
    }

    /**
     * Article viewHolder
     */
    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title = itemView.findViewById<TextView>(R.id.articleTitle)
        private val image = itemView.findViewById<ImageView>(R.id.articleImage)
        private val abstract = itemView.findViewById<TextView>(R.id.articleAbstract)
        private val body = itemView.findViewById<TextView>(R.id.articleBody)
        private val author = itemView.findViewById<TextView>(R.id.articleAuthor)
        private val progress = itemView.findViewById<ProgressBar>(R.id.progress)

        fun bind(itemDto: ItemDto) {

            // Reset view position when recycled
            itemView.scrollTo(0, 0)
            body.visibility = View.INVISIBLE
            progress.visibility = View.VISIBLE

            // Load image lazily in background
            itemDto.getMediumImage()?.let {
                Glide.with(fragment)
                        .load(it)
                        .crossFade(1000)
                        .centerCrop()
                        .into(image)
            }

            title.text = itemDto.title
            abstract.text = itemDto.abstract
            author.text = itemDto.author

            async {
                try {
                    val document = Jsoup.connect(itemDto.url).get()
                    val articleText = Jsoup.parse(document.html()).text()
                    Handler(Looper.getMainLooper()).post {
                        body.text = articleText
                        hideLoader()
                    }
                } catch (e: SocketTimeoutException) {
                    Snackbar.make(itemView, "No network available. Please comme back later later", Snackbar.LENGTH_SHORT).show()
                }
            }

        }

        private fun hideLoader() {
            body.visibility = View.VISIBLE
            progress.visibility = View.INVISIBLE
        }

        private fun ItemDto.getMediumImage(): String? {
            return images?.firstOrNull { it.format == Format.MEDIUM_THREE_BY_TWO }?.url
        }

    }

}
