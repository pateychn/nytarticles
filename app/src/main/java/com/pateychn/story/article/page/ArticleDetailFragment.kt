package com.pateychn.story.article.page

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.ShareActionProvider
import android.view.*
import com.pateychn.story.R
import com.pateychn.story.article.ArticlesViewModel
import com.pateychn.story.core.getViewModel
import com.pateychn.story.core.observe
import com.pateychn.story.dto.ItemDto


/**
 * Display articles list
 */
class ArticleDetailFragment : Fragment() {

    private lateinit var viewModel: ArticlesViewModel
    private lateinit var articlesRecyclerView: RecyclerView
    private var shareActionProvider: ShareActionProvider? = null

    private val articleAdapter: ArticleDetailAdapter by lazy {
        ArticleDetailAdapter(this)
    }

    /**
     * Fragment fabric
     */
    companion object {
        fun getInstance(): ArticleDetailFragment {
            return ArticleDetailFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_article_detail, container, false)
    }

    /**
     * Init view & observe data
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // RecyclerView initialisation
        activity?.let { fragmentActivity ->

            articlesRecyclerView = fragmentActivity.findViewById(R.id.articlesRecyclerView)
            PagerSnapHelper().attachToRecyclerView(articlesRecyclerView)

            articlesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    if ( newState == RecyclerView.SCROLL_STATE_IDLE) {
                        val position = (articlesRecyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                        if (position != -1) {
                            viewModel.listPosition.value = position
                        }
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })

            // Attach viewmodel
            viewModel = fragmentActivity.getViewModel()

            // Listen to articles list updates
            fragmentActivity.observe(viewModel.articles) {
                addArticlesToList(it)
            }

            // Set position
            fragmentActivity.observe(viewModel.listPosition) {
                scrollToArticle(it)
            }

            fragmentActivity.observe(viewModel.currentArticle) {
                setShareIntent(it)
            }

            (fragmentActivity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.toolbar_menu, menu)
        val shareItem = menu?.findItem(R.id.action_share)
        shareActionProvider = MenuItemCompat.getActionProvider(shareItem) as ShareActionProvider
        setShareIntent(viewModel.currentArticle.value)
    }

    private fun setShareIntent(itemDto: ItemDto?) {
        itemDto?.let {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, itemDto.url)
            shareActionProvider?.setShareIntent(shareIntent)
        }
    }

    /**
     * Position recyclerview on the selected article
     */
    private fun scrollToArticle(position: Int?) {
        position?.let {
            articlesRecyclerView.scrollToPosition(it)
        }
    }

    /**
     * Sends articleItem list to the adapter
     */
    private fun addArticlesToList(articlesList: List<ItemDto>?) {
        articlesList?.let {
            if (articlesRecyclerView.adapter==null) articlesRecyclerView.adapter = articleAdapter
            articleAdapter.setData(it.toMutableList())
        } ?: articleAdapter.clear()
    }

}