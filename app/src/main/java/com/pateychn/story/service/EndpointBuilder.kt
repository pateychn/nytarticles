package com.pateychn.story.service

import com.google.gson.*
import com.pateychn.story.core.ArticlesApp
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.lang.reflect.Type
import java.text.DateFormat
import java.util.*

//
// Helpers functions to help build a retrofit endpoint
//

/**
 * Returns an instance of retrofit endpoint
 */
fun OkHttpClient.Builder.usesCache(usesCache: Boolean): OkHttpClient.Builder {
    if ( usesCache ) {
        val httpCacheDirectory = File(ArticlesApp.getContext().cacheDir, "responses")
        val cacheSize = 1000L * 512L //  500 Ko
        val cache = Cache(httpCacheDirectory, cacheSize)
        this.cache(cache)
        this.addInterceptor { chain ->
            try {
                chain.proceed(chain.request())
            } catch (e: Exception) {
                val maxStale = 60 * 60 * 24 * 28 // 4-weeks
                val offLineRequest = chain.request().newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                        .build()
                chain.proceed(offLineRequest)
            }
        }
    }
    return this
}

/**
 * Enable requests & responses logging
 */
fun OkHttpClient.Builder.enableLogging(): OkHttpClient.Builder {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    this.addInterceptor(logging)
    return this
}

/**
 * Adds api key
 */
fun OkHttpClient.Builder.apiKey(apiKey: String): OkHttpClient.Builder {
    // Adds apiKey to url
    this.addInterceptor { chain ->
        val original = chain.request()
        val url = original
                .url()
                .newBuilder()
                .addQueryParameter("api-key", apiKey)
                .build()
        val request = original
                .newBuilder()
                .url(url)
                .build()
        chain.proceed(request)
    }
    return this
}

/**
 * Sets url endpoint
 */
inline fun <reified T> OkHttpClient.Builder.endPoint(endPoint: String): T {
    return Retrofit.Builder()
            .baseUrl(endPoint)
            .addConverterFactory(GsonConverterFactory.create(getGsonConverter()))
            .client(this.build())
            .build()
            .create(T::class.java)
}

/**
 * Converter object to interpret date format
 */
fun getGsonConverter(): Gson {
    return GsonBuilder()
            .registerTypeAdapter(Date::class.java, ArticleDateDeserializer())
            .setDateFormat(DateFormat.LONG)
            .create()
}

/**
 * Custom date deserializer implementation
 */
class ArticleDateDeserializer : JsonDeserializer<Date> {
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Date {
        return Date(json.asLong*1000)
    }
}
