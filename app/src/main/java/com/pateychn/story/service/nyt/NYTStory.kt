package com.pateychn.story.service.nyt

import com.google.gson.annotations.SerializedName
import com.pateychn.story.core.Format

/**
 * Articles data model
 */
data class Response (
        @SerializedName("results")
        val results: ArrayList<Result>
)

data class Result (
        @SerializedName("title")
        val title: String,
        @SerializedName("abstract")
        val abstract: String,
        @SerializedName("url")
        val url: String,
        @SerializedName("byline")
        val byline: String,
        @SerializedName("published_date")
        val publishedDate: String,
        @SerializedName("multimedia")
        val multimedia: ArrayList<Multimedia>

)

data class Multimedia (
        @SerializedName("url")
        val url: String,
        @SerializedName("type")
        val type: String,
        @SerializedName("format")
        val format: Format,
        @SerializedName("width")
        val width: Int,
        @SerializedName("height")
        val height: Int
)
