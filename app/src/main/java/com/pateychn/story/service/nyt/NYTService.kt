package com.pateychn.story.service.nyt

import com.pateychn.story.BuildConfig
import com.pateychn.story.endpoint.NYTEndpoint
import com.pateychn.story.service.apiKey
import com.pateychn.story.service.enableLogging
import com.pateychn.story.service.endPoint
import okhttp3.OkHttpClient
import retrofit2.Call


/**
 * App entry point for api
 *
 * singleton class
 */
class NYTService {

    private var retrofit: NYTEndpoint

    companion object {
        private var instance: NYTService? = null
        // Returns single service instance
        fun getService(): NYTService = instance
                ?: NYTService()
    }

    init {
        instance = this
        retrofit = OkHttpClient.Builder()
                .enableLogging()
                .apiKey(BuildConfig.APIKEY)
                .endPoint(BuildConfig.ENDPOINT)
    }

    /**
     * Request data
     * Asynchronous call
     *
     * @param page number to load for paged list
     * @param initCallback : callback interface with onSuccess/onError dsl support
     */
    fun getData(): Call<Response> {
        return retrofit.getData()
    }

}