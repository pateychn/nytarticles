package com.pateychn.story.dto

import com.pateychn.story.core.Format

/**
 * Articles data for display
 */
data class ItemDto(
        var title: String? = "",
        var abstract: String? = "",
        var url: String? = "",
        var author: String? = "",
        var publishedDate: String? = "",
        var images: List<ImageDto>? = null)

data class ImageDto(
        var url: String? = "",
        var type: String? = "",
        var format: Format = Format.STANDARD,
        val width: Int,
        val height: Int
)

