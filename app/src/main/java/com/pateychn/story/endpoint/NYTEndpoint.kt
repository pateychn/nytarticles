package com.pateychn.story.endpoint

import com.pateychn.story.service.nyt.Response
import retrofit2.Call
import retrofit2.http.GET


/**
 * Retrofit endpoint interface
 */
interface NYTEndpoint {

    @GET("home.json")
    fun getData(): Call<Response>

}