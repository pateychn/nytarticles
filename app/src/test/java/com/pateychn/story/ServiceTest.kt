package com.pateychn.story

import com.pateychn.story.core.get
import com.pateychn.story.core.toItemDto
import com.pateychn.story.service.nyt.NYTService
import org.junit.Assert
import org.junit.Test

/**
 * Service getData
 */
class ServiceTest {

    @Test
    fun checkData() {
        NYTService.getService().getData().get().apply {
            Assert.assertNotNull("response should not be null", this)
            this?.results?.let { articles ->
                articles
                        .map { it.toItemDto() }
                        .forEach {
                            println(it)
                            it.images?.forEach { println(it) }
                        }
            }
        }
    }

}